# Vault encrypt example #
sudo ansible-vault create yourfile.yaml

will ask you for password

enter key value pairs so username: something

when you exit the file is encrypted

you can do ansible-vault edit yourfile.yaml to edit it again if you wish

Playbook should look like this.

- hosts: kubernetesmaster

  tasks:

  - name: Include Vars
    include_vars: "/etc/ansible/vault/bellcpassword.yaml"

  - name: Change Users password
    #user: name=bellc password="{{bellcpassword}}" update_password=always
    user: name=bellc password="{{bellcpassword|password_hash('sha512')}}" update_password=always


some functions, like vmware_guest is deprecated, community.vmware.vmware_guest_info

ansible-galaxy collection install community.vmware

not as sudo.
