def jobNames = [
	'additional-identifier-api ',
	'additional-offence-api',
	'address-api',
	'address-assesment-api'
]
def branches = [
	'master',
	'v1',
	'v2',
	'v3'
]

nestedView('main-view') {
	branches.each { branchName ->
		views {
			nestedView('jobs-for-' + branchName) {
				views {
					listView(branchName + '-jobs') {
						jobs {
							jobNames.each { jobName ->
								name(jobName)

								for (repo in jobNames) {

    // Create the job
    job {
        name "${repo}"
        scm {
            git {
                remote {
                    url("http://sv-gitlab-10.meganexus.internal/TR/${repo}")

                    branch("master")
                }
            }
        }
        steps {
            shell("echo ${repoName}")
        }
    }
}
							}
						}
						columns {
							status()
							weather()
							name()
							lastSuccess()
							lastFailure()
							lastDuration()
							buildButton()
						}
					}
				}
			}
		}
    }

    }
