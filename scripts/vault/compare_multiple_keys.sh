#!/bin/bash
rm STE_ENTRYS
rm DEV_ENTRYS

STE_PATH="secret/ste/steccm01/"
#STE_ENTRY="support_password"
#STE_VALUE=`vault read -field=value $STE_PATH$STE_ENTRY`

DEV_PATH="secret/dev/devccm01/"
#DEV_ENTRY="support_password"
#DEV_VALUE=`vault read -field=value $DEV_PATH$DEV_ENTRY`

for i in `vault list $STE_PATH`; do echo $i | md5sum >> STE_ENTRYS; done
for ii in `vault list $DEV_PATH`; do echo $ii | md5sum >> DEV_ENTRYS; done

#while IFS= read -r dev_line; do
#	echo $dev_line
#done < <( cat STE_ENTRYS )


#while IFS= read -r dev_line; do
#        echo $ste_line

#done < <( cat DEV_ENTRYS )


#echo $STE_PATH$STE_ENTRY
#echo $STE_VALUE


#echo $DEV_PATH$DEV_ENTRY
#echo $DEV_VALUE

#printf "%s\t\n%s\t\n" "$STE_PATH$STE_ENTRY $DEV_PATH$DEV_ENTRY" "$STE_VALUE $DEV_VALUE"
#printf "%s\t\n%s\t\n" "$STE_PATH$STE_ENTRY $DEV_PATH$DEV_ENTRY" "`diff $STE_VALUE $DEV_VALUE`"


#sdiff <(echo "1234") <(echo "$DEV_VALUE")

#printf "%s\t\t\t\n" "$STE_PATH$STE_ENTRY $DEV_PATH$DEV_ENTRY"

diff -s -y <(cat STE_ENTRYS) <(cat DEV_ENTRYS)

#diff -y --old-group-format=$'\e[0;31m%<\e[0m' \
#     --new-group-format=$'\e[0;31m%>\e[0m' \
#     --unchanged-group-format=$'\e[0;32m%=\e[0m' \
#     <(cat STE_ENTRYS) <(cat DEV_ENTRYS)


#rm STE_ENTRYS
#rm DEV_ENTRYS
