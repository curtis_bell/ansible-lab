resource "azurerm_virtual_network" "vnet" {
  name                =  "bellc-dmo-network"
  address_space       = ["10.92.0.0/24"]
  location            = "${var.global_azure_location}"
  resource_group_name = "${var.resourcegroup}"

#  subnet {
#    name = "address_space_vnet"
#    address_prefix = "${var.dmo-dmz-vnet-scheme["address_space_vnet"]}"
#    }

#  subnet {
#    name = "vm_address"
#    address_prefix = "${var.dmo-dmz-vnet-scheme["vm_address"]}"
#    }


#  subnet {
#    name = "address_prefix-subnet-agw"      address_prefix = "${var.dmo-dmz-vnet-scheme["address_prefix-subnet-agw"]}"
#    }
#
#  subnet {
#    name = "lb_address"
#    address_prefix = "${var.dmo-dmz-vnet-scheme["lb_address"]}"
#    }


}

resource "azurerm_subnet" "address_space_vnet" {
  name                 = "address_space_vnet"
  resource_group_name  = "${var.resourcegroup}"
  virtual_network_name = "${azurerm_virtual_network.vnet.name}"
  address_prefix = "${var.dmo-dmz-vnet-scheme["address_space_vnet"]}"
}

resource "azurerm_subnet" "vm_address" {
  name                 = "${var.identifiers["subnet"]}-${var.environments["dmo"]}-${upper(local.env["tier"])}-VM-01"
  resource_group_name  = "${var.resourcegroup}"
  virtual_network_name = "${azurerm_virtual_network.vnet.name}"
  address_prefix = "${var.dmo-dmz-vnet-scheme["vm_address"]}"
}

resource "azurerm_subnet" "address_prefix-subnet-agw" {
  name                 = "address_prefix-subnet-agw"
  resource_group_name  = "${var.resourcegroup}"
  virtual_network_name = "${azurerm_virtual_network.vnet.name}"
  address_prefix = "${var.dmo-dmz-vnet-scheme["address_prefix-subnet-agw"]}"
 }

resource "azurerm_subnet" "lb_address" {
  name                 = "${var.identifiers["subnet"]}-${var.environments["dmo"]}-${upper(local.env["tier"])}-LB-01"
  resource_group_name  = "${var.resourcegroup}"
  virtual_network_name = "${azurerm_virtual_network.vnet.name}"
  address_prefix = "${var.dmo-dmz-vnet-scheme["lb_address"]}"
}
