aws ec2 authorize-security-group-ingress --group-id sg-0af42b99ee56e7262 --protocol tcp --port 22 --cidr 0.0.0.0/0
aws ec2 authorize-security-group-ingress --group-id sg-0af42b99ee56e7262 --protocol tcp --port 5000 --cidr 0.0.0.0/0
aws ec2 authorize-security-group-ingress --group-id sg-0af42b99ee56e7262 --protocol tcp --port 2379  --cidr 0.0.0.0/0
aws ec2 authorize-security-group-ingress --group-id sg-0af42b99ee56e7262 --protocol tcp --port 8080 --cidr 0.0.0.0/0
aws ec2 authorize-security-group-ingress --group-id sg-0af42b99ee56e7262 --protocol tcp --port 3306 --cidr 0.0.0.0/0
aws ec2 authorize-security-group-ingress --group-id sg-0af42b99ee56e7262 --protocol tcp --port 31814 --cidr 0.0.0.0/0
